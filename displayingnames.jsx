import React from 'react';

class Displayname extends React.Component {
    constructor(props) {
        super(props);
        this.state = { name: 'Ajitha' };
        this.welcome=this.welcome.bind(this)
    }

    welcome(props) {
        return <h1>Hello, {this.state.name}</h1>;
    }

    render() {

        return (
            <div>
                {this.welcome()}
                {this.props.text}
            </div>
        );
    }
}
export default Displayname;